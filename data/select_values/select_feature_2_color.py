select_feature_2_color = [
    {'label': 'nichts ausgewählt', 'value': 'na'},
    {'label': 'Kontinent', 'value': 'continent'},
    {'label': 'Land', 'value': 'country'},
    {'label': 'Branche', 'value': 'sector'},
    {'label': 'Modalität', 'value': 'repayment_interval'},
    {'label': 'Zielbetragkat.', 'value': 'loan_amount_cat'},
    {'label': 'Teamzus.setz.', 'value': 'team_composition'},
    {'label': 'Anz. Kreditgeber', 'value': 'lender_cat'},
    {'label': 'Auszahl.zeitr.', 'value': 'term_in_months_cat'},
]
