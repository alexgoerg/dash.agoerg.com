import plotly.graph_objects as go
import pandas as pd

def get_plot_21_1_pie(i_df, i_feature, i_measure, i_continents, i_sectors, i_countries, i_repayment_interval,i_loan_amount_cat, i_team_composition, i_lender_cat, i_term_in_months_cat, i_select_feature, i_select_measure):
    if isinstance(i_continents, list) & isinstance(i_sectors, list) & isinstance(i_countries, list) & isinstance(i_repayment_interval, list) & isinstance(i_loan_amount_cat, list)& isinstance(i_team_composition, list)& isinstance(i_lender_cat, list)& isinstance(i_term_in_months_cat, list):
        l_df = i_df.loc[i_df.continent.isin(i_continents) & i_df.sector.isin(
            i_sectors) & 
            i_df.country.isin(i_countries) 
            & i_df.repayment_interval.isin(i_repayment_interval)
            & i_df.loan_amount_cat.isin(i_loan_amount_cat)
            & i_df.team_composition.isin(i_team_composition)
            & i_df.lender_cat.isin(i_lender_cat)
            & i_df.term_in_months_cat.isin(i_term_in_months_cat)].copy()
        if i_measure in ['loan_amount','funded_amount']:
            l_aggregation = 'sum'
            l_label_unit = 'in $'
        else:
            l_aggregation = 'size'
            l_label_unit = ''
        #
        df_sector_sum_funded_amount = l_df.groupby(
            by=[i_feature], as_index=False).agg({i_measure: l_aggregation})
        # sort
        df_sector_sum_funded_amount = df_sector_sum_funded_amount.sort_values(
            by=i_measure, ascending=False).copy()
        #
        if i_feature == 'continent':
            df_color = pd.DataFrame({'continent': ['Asia', 'Africa', 'South America', 'North America', 'Europe', 'Oceania'],
                                    'color': ['red', 'blue', 'green', 'yellow', 'orange', 'grey']})
            df_sector_sum_funded_amount = df_sector_sum_funded_amount.merge(
                df_color, on="continent")
        #
            fig = go.Figure(data=[
                go.Pie(labels=df_sector_sum_funded_amount['continent'],
                    values=df_sector_sum_funded_amount[i_measure],
                    marker_colors=df_sector_sum_funded_amount['color'],
                    direction ='clockwise')
            ])
        elif i_feature == 'repayment_interval':
            df_color = pd.DataFrame({'repayment_interval': ['monthly', 'bullet', 'irregular', 'weekly'],
                                    'color': ['green', 'blue', 'red', 'orange']})
            df_sector_sum_funded_amount = df_sector_sum_funded_amount.merge(
                df_color, on="repayment_interval")
        
            fig = go.Figure(data=[
            go.Pie(labels=df_sector_sum_funded_amount['repayment_interval'],
                values=df_sector_sum_funded_amount[i_measure],
                marker_colors=df_sector_sum_funded_amount['color'],
                direction ='clockwise')
        ])
        else: 
            fig = go.Figure(data=[
                go.Pie(labels=df_sector_sum_funded_amount[i_feature],
                    values=df_sector_sum_funded_amount[i_measure],
                    direction ='clockwise')
            ])
        #
        fig.update_traces(
            textposition='inside',
            texttemplate='%{label}<br>%{value:.2s}<br>%{percent}',
        )
        #
        l_lable_feature = ''
        for elem in i_select_feature:
            if elem['value'] == i_feature:
                l_lable_feature = elem['label']
        #
        l_lable_measure = ''
        for elem in i_select_measure:
            if elem['value'] == i_measure:
                l_lable_measure = elem['label']
        fig.update_layout(
            title={
                'text': l_lable_measure + " pro " + l_lable_feature + " " + l_label_unit,
                'y': 0.94,
                'x': 0.5,
                'xanchor': 'center',
                'yanchor': 'top',
                'font_size': 15
            },
            showlegend=False,
            height=250,
            margin=dict(l=20, r=20, t=35, b=10),
            paper_bgcolor="LightSteelBlue",
        )
        return fig
    else:
        return {}