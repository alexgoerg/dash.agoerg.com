import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
import os

def get_plot_bar(i_df,
                 i_feature,
                 i_feature_color,
                 i_measure,
                 i_continents,
                 i_sectors,
                 i_countries,
                 i_repayment_interval,
                 i_loan_amount_cat,
                 i_team_composition, i_lender_cat, i_term_in_months_cat,
                 i_select_feature,
                 i_select_feature_color,
                 i_select_measure):
    if  (isinstance(i_sectors, list) 
    & isinstance(i_repayment_interval, list)
    & isinstance(i_loan_amount_cat, list)
    & isinstance(i_team_composition, list)
    & isinstance(i_lender_cat, list)
    & isinstance(i_term_in_months_cat, list)
    & isinstance(i_continents, list)
    & isinstance(i_countries, list)):
        l_df = i_df.loc[i_df.sector.isin(i_sectors) &
                        i_df.repayment_interval.isin(i_repayment_interval) &
                        i_df.loan_amount_cat.isin(i_loan_amount_cat) &
                        i_df.team_composition.isin(i_team_composition) &
                        i_df.lender_cat.isin(i_lender_cat) &
                        i_df.term_in_months_cat.isin(i_term_in_months_cat) &
                        i_df.continent.isin(i_continents) &
                        i_df.country.isin(i_countries)].copy()
        #
        if i_measure in ['loan_amount', 'funded_amount']:
            l_aggregation = 'sum'
            l_label_unit = 'in $'
        else:
            l_aggregation = 'size'
            l_label_unit = ''
        #
        l_lable_feature = ''
        for elem in i_select_feature:
            if elem['value'] == i_feature:
                l_lable_feature = elem['label']
        #
        l_lable_feature_color = ''
        for elem in i_select_feature_color:
            # print("a")
            # print(isinstance(i_feature_color, str))
            if isinstance(i_feature_color, str):
                if (elem['value'] == i_feature_color) & (i_feature_color != 'na'):
                    l_lable_feature_color = "und " + elem['label'] + " "
        #
        #
        l_lable_measure = ''
        for elem in i_select_measure:
            if elem['value'] == i_measure:
                l_lable_measure = elem['label']
        #
        if len(l_df) > 0:
            if i_feature_color != 'na':
                l_show_legend = False
                dict_color = {}
                dict_labels = {}
                df_sector_size = l_df.groupby(by=[i_feature, i_feature_color]).agg({
                    i_measure: l_aggregation})
                df_sector_size.reset_index(inplace=True)
                df_sector_size = df_sector_size.sort_values(
                    by=i_measure, ascending=False).copy()
                df_sector_size = df_sector_size.loc[df_sector_size[i_measure] > 0].copy()
                # os.system("clear")
                # print("df_sector_size")
                # print(df_sector_size)
                if i_feature_color == 'continent':
                    l_show_legend = True
                    dict_color = {
                        'Asia': 'red',
                        'Africa': 'blue',
                        'South America': 'green',
                        'North America': 'yellow',
                        'Europe': 'orange',
                        'Oceania': 'grey',
                    }
                elif i_feature_color == 'repayment_interval':
                    l_show_legend = True
                    dict_color = {
                        'monthly': 'green',
                        'bullet': 'blue',
                        'irregular': 'red',
                        'weekly': 'orange'
                    }
                    dict_labels={"repayment_interval": "Modalität"}
                elif i_feature_color == 'loan_amount_cat':
                    l_show_legend = True
                    dict_color = {
                        'bis 1k $': 'yellow',
                        'bis 10k $': 'green',
                        'bis 100k $': 'blue'
                    }
                    dict_labels={"loan_amount_cat": "Zielbetragskat."}
                elif i_feature_color == 'team_composition':
                    l_show_legend = True
                    dict_color = {
                        'female': 'green',
                        'male': 'red',
                        'mixed': 'yellow',
                        'not specified': 'black'
                    }
                    dict_labels={"team_composition": "Teamzus.setz."}
                elif i_feature_color == 'term_in_months_cat':
                    l_show_legend = True
                    dict_color = {
                        'bis 1 J.': 'green',
                        'bis 2 J.': 'yellow',
                        'bis 3. J': 'orange',
                        'bis 15 J.': 'red'
                    }
                    dict_labels={"term_in_months_cat": "Auszahl.zeitr."}
                elif i_feature_color == 'lender_cat':
                    l_show_legend = True
                    dict_color = {
                        '<=10': 'red',
                        '<=100': 'yellow',
                        '<=1000': 'green',
                        '<=3000': 'blue'
                    }
                    dict_labels={"lender_cat": "Anz. Kreditgeber"}
                # 
                # print(df_sector_size)
                fig = px.bar(df_sector_size,
                                x=i_feature,
                                y=i_measure,
                                color=i_feature_color,
                                color_discrete_map=dict_color,
                                labels=dict_labels)
            # wenn farbe nicht gesetzt
            else:
                l_show_legend = False
                df_sector_size = l_df.groupby(by=[i_feature]).agg(
                    {i_measure: l_aggregation})
                df_sector_size.reset_index(inplace=True)
                df_sector_size = df_sector_size.sort_values(
                    by=i_measure, ascending=False).copy()
                df_sector_size.loc[:, [i_feature]
                                   ] = df_sector_size[i_feature].str[:15].copy()
                df_sector_size = df_sector_size.loc[df_sector_size[i_measure] > 0].copy()
                # df_sector_size = df_sector_size.loc[0:15].copy()
                os.system("clear")
                print("df_sector_size")
                print(df_sector_size)
                fig = px.bar(df_sector_size,
                             x=i_feature,
                             y=i_measure)
        else:
            """"""
        if len(l_df) > 0:
            fig.update_layout(
                title={
                    'text': l_lable_measure + " pro " + l_lable_feature + " " + l_lable_feature_color + l_label_unit,
                    'y': 0.94,
                    'x': 0.5,
                    'xanchor': 'center',
                    'yanchor': 'top',
                    'font_size': 15
                },
                showlegend=l_show_legend,
                height=290,
                margin=dict(l=0, r=50, t=40, b=0),
                paper_bgcolor="#00BFFF",
                xaxis={'categoryorder': 'total descending'}
            )
            # fig.update_xaxes(range=(-0.5, 20.5))
            # print(fig.layout)
            fig.layout.xaxis.title.text = ''
            fig.layout.yaxis.title.text = ''
            return fig
        else:
            return {}
    else:
        return {}
