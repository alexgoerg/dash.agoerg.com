import plotly.graph_objects as go
import numpy as np


def get_plot_11_1_datatable(i_df, i_continents, i_sectors, i_countries, 
i_repayment_interval, i_loan_amount_cat, i_team_composition, i_lender_cat, i_term_in_months_cat):
    if isinstance(i_continents, list) & isinstance(i_sectors, list) & isinstance(i_countries, list) & isinstance(i_repayment_interval, list) & isinstance(i_loan_amount_cat, list)& isinstance(i_team_composition, list)& isinstance(i_lender_cat, list)& isinstance(i_term_in_months_cat, list):
        l_df = i_df.loc[i_df.continent.isin(i_continents) & i_df.sector.isin(
            i_sectors) & 
            i_df.country.isin(i_countries) 
            & i_df.repayment_interval.isin(i_repayment_interval)
            & i_df.loan_amount_cat.isin(i_loan_amount_cat)
            & i_df.team_composition.isin(i_team_composition)
            & i_df.lender_cat.isin(i_lender_cat)
            & i_df.term_in_months_cat.isin(i_term_in_months_cat)].copy()
        quantity_of_projects = len(l_df.loc[:, ['country']])
        quantity_of_projects_str = "{:,}".format(quantity_of_projects)
        #
        loan_amount = l_df.loc[:, ['loan_amount']].sum()
        loan_amount_str = "{:,} USD".format(int(loan_amount[0]))
        #
        funded_amount = l_df.loc[:, ['funded_amount']].sum()
        if loan_amount[0] == 0:
            l_loan_amount = 1
        else:
            l_loan_amount = loan_amount[0]
        funded_percent = "{:,} %".format(
            np.round(funded_amount[0] / l_loan_amount * 100, 1))
        #
        fig = go.Figure(
            data=[
                go.Table(
                    # header=dict(
                    #     values=['Zielbetrag', 'gefördert', 'Anzahl Projekte']),
                    cells=dict(values=[
                        ['Zielbetrag', 'gefördert', 'Anzahl Projekte'],
                        [loan_amount_str, funded_percent, quantity_of_projects_str]
                    ],
                        height=40,
                        align='center',
                        font=dict(color='black', size=30),
                        font_color=[['green','black','black']]
                    )
                )
            ])
        fig.update_layout(
            margin=dict(l=5, r=5, t=40, b=5),
            paper_bgcolor="#6495ED",
            showlegend=False,
            height=250,
            title={
                'y': 0.94,
                'x': 0.5,
                'xanchor': 'center',
                'yanchor': 'top',
                'font_size': 15}
        )
        return fig
    else:
        return {}
