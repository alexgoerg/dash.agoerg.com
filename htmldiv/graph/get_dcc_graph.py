import dash_html_components as html
import dash_core_components as dcc


def get_dcc_graph(i_id, i_width='100%'):
    return html.Div(
        style={'width': i_width,
               'padding': '0px',
               'box-sizing': 'border-box',
               'border': '0px solid red'},
        children=[
            dcc.Graph(
                id=i_id)
        ]
    )

# ,
#                'display': 'flex',
#                'flex-wrap': 'wrap',
#                'align-items': 'center',
#                'justify-content': 'center'
