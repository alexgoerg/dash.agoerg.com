import dash_core_components as dcc

def get_check_boxes(i_id,
                    i_dropdown_continent,
                    i_dropdown_continent_values,
                    i_padding_bottom = '0px'):
    return dcc.Checklist(id=i_id,
                         options=i_dropdown_continent,
                         value=i_dropdown_continent_values,
                         labelStyle={
                             'display': 'block',
                             'cursor': 'pointer'
                         },
                         inputStyle={
                             'cursor': 'pointer'
                         },
                         style={
                             'padding-bottom': i_padding_bottom
                         })
