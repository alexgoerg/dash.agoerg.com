import dash_core_components as dcc

def get_dropdown_multi_false_without_value(
    i_id, 
    i_options, 
    i_width = '100%',
    i_margin_top= '0px',
    i_margin_bottom= '0px',
    i_padding_bottom = '0px'):
    return dcc.Dropdown(id=i_id,
                 options=i_options,
                 multi=False,
                 style={
                     'display': 'block',
                     'cursor': 'pointer',
                     'padding-bottom': i_padding_bottom,
                     'margin-top': i_margin_top,
                     'margin-bottom': i_margin_bottom,
                     'width': i_width
                })