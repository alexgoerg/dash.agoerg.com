import dash_core_components as dcc

def get_dropdown_multi_false(i_id, i_options, i_value, i_padding_bottom = '0px'):
    return dcc.Dropdown(id=i_id,
                 options=i_options,
                 value=i_value,
                 multi=False,
                 style={
                     'display': 'block',
                     'cursor': 'pointer',
                     'padding-bottom': i_padding_bottom
                })