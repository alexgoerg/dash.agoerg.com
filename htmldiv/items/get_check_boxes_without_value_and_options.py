import dash_core_components as dcc

def get_check_boxes_without_value_and_options(i_id,
                    i_background_color='grey',
                    i_margin_bottom = '0px',):
    return dcc.Checklist(id=i_id,
                         labelStyle={
                             'display': 'block',
                             'cursor': 'pointer'
                         },
                         inputStyle={
                             'cursor': 'pointer'
                         },
                         style={
                             'padding-bottom': i_margin_bottom,
                             'background-color': i_background_color
                         })
