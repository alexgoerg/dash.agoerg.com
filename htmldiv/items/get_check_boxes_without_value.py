import dash_core_components as dcc

def get_check_boxes_without_value(i_id,
                    i_dropdown_continent,
                    i_background_color='grey',
                    i_margin_bottom = '0px',):
    return dcc.Checklist(id=i_id,
                         options=i_dropdown_continent,
                         labelStyle={
                             'display': 'block',
                             'cursor': 'pointer'
                         },
                         inputStyle={
                             'cursor': 'pointer'
                         },
                         style={
                             'margin-bottom': i_margin_bottom,
                             'background-color': i_background_color
                         })
