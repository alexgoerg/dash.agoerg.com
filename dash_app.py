import pandas as pd
import dash
from dash.dependencies import Input, Output, State
import dash_html_components as html
import dash_core_components as dcc
import os
from flask import Flask
# from filelock import FileLock
#
from plot.get_plot_11_1_datatable import get_plot_11_1_datatable
from plot.get_plot_21_1_pie import get_plot_21_1_pie
from plot.get_plot_bar import get_plot_bar
#
from function.get_options import get_options
from function.get_values import get_values
#
from htmldiv.items.get_check_boxes_without_value import get_check_boxes_without_value
from htmldiv.items.get_check_boxes_without_value_and_options import get_check_boxes_without_value_and_options
from htmldiv.items.get_dropdown_multi_false_without_value import get_dropdown_multi_false_without_value
#
from htmldiv.graph.get_dcc_graph import get_dcc_graph
#
from data.select_values.select_feature_1 import select_feature_1
from data.select_values.select_feature_2_x import select_feature_2_x
from data.select_values.select_feature_2_color import select_feature_2_color
from data.select_values.select_measure_1 import select_measure_1
from data.select_values.select_measure_2 import select_measure_2
#
from function.get_feature import get_feature
from function.get_feature_value import get_feature_value
from function.get_saved_data_feature_value import get_saved_data_feature_value
from function.save_data_feature_value import save_data_feature_value
from function.get_n_clicks import get_n_clicks
#
g_df = pd.read_pickle('data/p2p_data.pkl')
g_df.loc[:, ['country']] = g_df['country'].str[:15].copy()
g_df_grouped = g_df.groupby(
    by=['continent', 'country'], as_index=False).agg({'loan_amount': 'sum'})
g_df_grouped.sort_values(by="loan_amount", ascending=False, inplace=True)
g_df_grouped.loc[:, ['country']] = g_df_grouped['country'].str[:15].copy()
#

l_select_feature_1 = select_feature_1
l_select_feature_2_x = select_feature_2_x
l_select_feature_2_color = select_feature_2_color
l_select_measure_1 = select_measure_1
l_select_measure_2 = select_measure_2
#
dropdown_continent = get_options(g_df, 'continent')
dropdown_sector = get_options(g_df, 'sector')
dropdown_repayment_interval = get_options(g_df, 'repayment_interval')
dropdown_loan_amount_cat = get_options(g_df, 'loan_amount_cat')
dropdown_team_composition = get_options(g_df, 'team_composition')
dropdown_lender_cat = get_options(g_df, 'lender_cat')
dropdown_term_in_months_cat = get_options(g_df, 'term_in_months_cat')
#
#
g_all_continent_values = get_values(g_df, 'continent')
g_all_country_values = get_values(g_df, 'country')
#
g_all_sector_values = get_values(g_df, 'sector')
g_all_repayment_interval_values = get_values(g_df, 'repayment_interval')
g_all_loan_amount_cat_values = get_values(g_df, 'loan_amount_cat')
g_all_team_composition_values = get_values(g_df, 'team_composition')
g_all_lender_cat_values = get_values(g_df, 'lender_cat')
g_all_term_in_months_cat_values = get_values(g_df, 'term_in_months_cat')
#
# Initialise the app
server = Flask(__name__)
app = dash.Dash(__name__,
                server=server,
                url_base_pathname="/")
# app.config.suppress_callback_exceptions = True
# os.system('clear')
# Define the app
app.layout = html.Div(
    children=[
        html.Div(style={'display': 'flex',
                        'flex-wrap': 'wrap',
                        'padding': '0px',
                        'border': '0px solid green',
                        'height': '100vh'},  # Define the row element
                 children=[
            dcc.Store(id='g_n_clicks_sector', storage_type='local'),
            dcc.Store(id='g_n_clicks_repayment_interval',
                      storage_type='local'),
            dcc.Store(id='g_n_clicks_loan_amount_cat', storage_type='local'),
            dcc.Store(id='g_n_clicks_team_composition', storage_type='local'),
            dcc.Store(id='g_n_clicks_lender_cat', storage_type='local'),
            dcc.Store(id='g_n_clicks_term_in_months_cat', storage_type='local'),
            dcc.Store(id='g_n_clicks_continent', storage_type='local'),
            dcc.Store(id='g_n_clicks_country', storage_type='local'),
            # left element
            html.Div(style={'width': '20%',
                            'padding': '5px',
                            'box-sizing': 'border-box',
                            'background-color': '#aaaaaa',
                            'height': '100%'},
                     children=html.Div(style={
                         'display': 'flex',
                         'flex-wrap': 'wrap'},
                children=[html.Div(style={'width': 'calc(50% - 2px)',
                                          'padding': '0px',
                                          'box-sizing': 'border-box',
                                          'background-color': '#aaaaaa',
                                          'height': '100%',
                                          'margin-right': '2px'},
                                   children=[
                    get_dropdown_multi_false_without_value(
                        i_id='feature_1', i_options=l_select_feature_1)]),
                         html.Div(style={'width': 'calc(50% - 2px)',
                                         'padding': '0px',
                                         'box-sizing': 'border-box',
                                         'background-color': '#aaaaaa',
                                         'height': '100%',
                                         'margin-right': '2px'},
                                  children=[get_dropdown_multi_false_without_value(
                                      i_id='measure_1', i_options=l_select_measure_1)]),
                         get_dropdown_multi_false_without_value(
                    i_id='feature_2_x', i_options=l_select_feature_2_x, i_margin_top='5px'),
                         html.Div(style={'width': 'calc(50% - 2px)',
                                         'padding': '0px',
                                         'box-sizing': 'border-box',
                                         'background-color': '#aaaaaa',
                                         'height': '100%',
                                         'margin-right': '2px'},
                                  children=[get_dropdown_multi_false_without_value(
                                      i_id='feature_2_color', i_options=l_select_feature_2_color)]),
                         html.Div(style={'width': 'calc(50% - 2px)',
                                         'padding': '0px',
                                         'box-sizing': 'border-box',
                                         'background-color': '#aaaaaa',
                                         'height': '100%',
                                         'margin-right': '2px'},
                                  children=[get_dropdown_multi_false_without_value(
                                      i_id='feature_2_2_color', i_options=l_select_feature_2_color)]),
                         html.Div(style={'width': 'calc(50% - 2px)',
                                         'padding': '0px',
                                         'box-sizing': 'border-box',
                                         'background-color': '#aaaaaa',
                                         'height': '100%',
                                         'margin-right': '2px'},
                                  children=[get_dropdown_multi_false_without_value(
                                      i_id='feature_2_3_color', i_options=l_select_feature_2_color)]),
                         html.Div(style={'width': 'calc(50% - 2px)',
                                         'padding': '0px',
                                         'box-sizing': 'border-box',
                                         'background-color': '#aaaaaa',
                                         'height': '100%',
                                         'margin-right': '2px'},
                                  children=[get_dropdown_multi_false_without_value(
                                      i_id='feature_2_4_color', i_options=l_select_feature_2_color)]),
                         get_dropdown_multi_false_without_value(
                    i_id='measure_2', i_options=l_select_measure_2, i_margin_bottom='3px'),
                         #
                         #
                         #
                         #
                         #
                         #
                         html.Div(style={'width': 'calc(50% - 2px)',
                                         'padding': '0px',
                                         'box-sizing': 'border-box',
                                         'background-color': '#aaaaaa',
                                         'height': '100%',
                                         'margin-right': '2px'},
                                  children=[html.Button('Branche', id='de_select_sector', style={'width': '100%'}),
                                            get_check_boxes_without_value(
                                      'sector', dropdown_sector, i_background_color='green', i_margin_bottom='10px'),
                             html.Button(
                                      'Kontinent', id='de_select_continent', style={'width': '100%'}),
                             get_check_boxes_without_value(
                                      'continent', dropdown_continent, i_background_color='red', i_margin_bottom='10px'),
                             html.Button(
                                      'Land', id='de_select_country', style={'width': '100%'}),
                             get_check_boxes_without_value_and_options(
                                      'country', i_background_color='orange'),
                             get_check_boxes_without_value_and_options('dummy', i_background_color='white')]),
                         html.Div(style={'width': 'calc(50% - 2px)',
                                         'padding': '0px',
                                         'box-sizing': 'border-box',
                                         'background-color': '#aaaaaa',
                                         'height': '100%',
                                         'margin-left': '2px'},
                                  children=[
                             html.Button(
                                 'Modalität', id='de_select_repayment_interval', style={'width': '100%'}),
                             get_check_boxes_without_value(
                                 'repayment_interval', dropdown_repayment_interval, i_background_color='#6495ED', i_margin_bottom='10px'),
                             html.Button(
                                 'Zielbetragkat.', id='de_select_loan_amount_cat', style={'width': '100%'}),
                             get_check_boxes_without_value(
                                 'loan_amount_cat', dropdown_loan_amount_cat, i_background_color='#6495ED', i_margin_bottom='10px'),
                             html.Button(
                                 'Teamzus.setz.', id='de_select_team_composition', style={'width': '100%'}),
                             get_check_boxes_without_value(
                                 'team_composition', dropdown_team_composition, i_background_color='#6495ED', i_margin_bottom='10px'),
                             html.Button(
                                 'Anz. Kreditgeber', id='de_select_lender_cat', style={'width': '100%'}),
                             get_check_boxes_without_value(
                                 'lender_cat', dropdown_lender_cat, i_background_color='#6495ED', i_margin_bottom='10px'),
                             html.Button(
                                 'Auszahl.zeitr.', id='de_select_term_in_months_cat', style={'width': '100%'}),
                             get_check_boxes_without_value('term_in_months_cat', dropdown_term_in_months_cat, i_background_color='#6495ED', i_margin_bottom='10px')])]
            )
            ),
            # right element
            html.Div(
                style={'width': '80%', 'padding': '0px',
                       'box-sizing': 'border-box',
                       'border': '0px solid red',
                       'background-color': '#dddddd', },
                children=html.Div(
                    style={'display': 'flex',
                           'flex-wrap': 'wrap'},
                    children=[
                        get_dcc_graph(i_id='21_1_pie', i_width='25%'),
                        get_dcc_graph(i_id='11_sum', i_width='75%'),
                        get_dcc_graph(i_id='3_1_bar', i_width='50%'),
                        get_dcc_graph(i_id='3_2_bar', i_width='50%'),
                        get_dcc_graph(i_id='3_3_bar', i_width='50%'),
                        get_dcc_graph(i_id='3_4_bar', i_width='50%')

                    ]
                )
            )
        ])
    ])
#

########################################################################################
# Plots
########################################################################################
@app.callback(Output('11_sum', 'figure'),
              [Input('continent', 'value'),
              Input('sector', 'value'),
              Input('country', 'value'),
              Input('repayment_interval', 'value'),
              Input('loan_amount_cat', 'value'),
              Input('team_composition', 'value'),
              Input('lender_cat', 'value'),
              Input('term_in_months_cat', 'value')])
def update(i_continents, i_sectors, i_countries,
           i_repayment_interval,
           i_loan_amount_cat,
           i_team_composition,
           i_lender_cat,
           i_term_in_months_cat):
    return get_plot_11_1_datatable(g_df, i_continents, i_sectors, i_countries,
                                   i_repayment_interval,
                                   i_loan_amount_cat,
                                   i_team_composition,
                                   i_lender_cat,
                                   i_term_in_months_cat)

@app.callback(Output('21_1_pie', 'figure'),
              [Input('feature_1', 'value'),
              Input('measure_1', 'value'),
              Input('continent', 'value'),
              Input('sector', 'value'),
              Input('country', 'value'),
              Input('repayment_interval', 'value'),
              Input('loan_amount_cat', 'value'),
              Input('team_composition', 'value'),
              Input('lender_cat', 'value'),
              Input('term_in_months_cat', 'value')])
def update(i_feature_1, i_measure_1, i_continents, i_sectors, i_countries, i_repayment_interval, i_loan_amount_cat, i_team_composition, i_lender_cat, i_term_in_months_cat):
    if i_feature_1:
        return get_plot_21_1_pie(g_df,
                                 i_feature_1,
                                 i_measure_1,
                                 i_continents,
                                 i_sectors,
                                 i_countries,
                                 i_repayment_interval,
                                 i_loan_amount_cat,
                                 i_team_composition, i_lender_cat, i_term_in_months_cat,
                                 l_select_feature_1,
                                 l_select_measure_1)
    else:
        return {}

l_items = ['feature_2_color','feature_2_2_color','feature_2_3_color','feature_2_4_color']
l_plot_ids = ['3_1_bar','3_2_bar','3_3_bar','3_4_bar']
for index in range(len(l_items)):
    @app.callback(Output(l_plot_ids[index], 'figure'),
                [Input('feature_2_x', 'value'),
                Input(l_items[index], 'value'),
                Input('measure_2', 'value'),
                Input('sector', 'value'),
                Input('continent', 'value'),
                Input('country', 'value'),
                Input('repayment_interval', 'value'),
                Input('loan_amount_cat', 'value'),
                Input('team_composition', 'value'),
                Input('lender_cat', 'value'),
                Input('term_in_months_cat', 'value')])
    def update(i_feature_2_x, i_feature_2_color, i_measure_2, i_sectors, i_continents, i_countries, i_repayment_interval, i_loan_amount_cat,
            i_team_composition, i_lender_cat, i_term_in_months_cat):
        return get_plot_bar(g_df,
                            i_feature_2_x,
                            i_feature_2_color,
                            i_measure_2,
                            i_continents,
                            i_sectors,
                            i_countries,
                            i_repayment_interval,
                            i_loan_amount_cat, i_team_composition, i_lender_cat, i_term_in_months_cat,
                            l_select_feature_2_x,
                            l_select_feature_2_color,
                            l_select_measure_2)


########################################################################################
# Items - Features
########################################################################################


@app.callback(
    Output('feature_1', 'value'),
    Input('feature_1', 'value'))
def set_feature(i_feature):
    return get_feature('feature_1', i_feature)


@app.callback(
    Output('measure_1', 'value'),
    Input('measure_1', 'value'))
def set_feature(i_feature):
    return get_feature('measure_1', i_feature)


@app.callback(
    Output('feature_2_x', 'value'),
    Input('feature_2_x', 'value'))
def set_feature(i_feature):
    return get_feature('feature_2_x', i_feature)


@app.callback(
    Output('feature_2_color', 'value'),
    Input('feature_2_color', 'value'))
def set_feature(i_feature):
    return get_feature('feature_2_color', i_feature)


@app.callback(
    Output('feature_2_2_color', 'value'),
    Input('feature_2_2_color', 'value'))
def set_feature(i_feature):
    return get_feature('feature_2_2_color', i_feature)


@app.callback(
    Output('feature_2_3_color', 'value'),
    Input('feature_2_3_color', 'value'))
def set_feature(i_feature):
    return get_feature('feature_2_3_color', i_feature)


@app.callback(
    Output('feature_2_4_color', 'value'),
    Input('feature_2_4_color', 'value'))
def set_feature(i_feature):
    return get_feature('feature_2_4_color', i_feature)


@app.callback(
    Output('measure_2', 'value'),
    Input('measure_2', 'value'))
def set_feature(i_feature):
    return get_feature('measure_2', i_feature)
#
#
#
#
#
#
#
#
#
#
#
#
#
########################################################################################
# Items - FeatureValues
########################################################################################


@app.callback(Output('g_n_clicks_sector', 'data'),
              Input('de_select_sector', 'n_clicks'))
def on_click(n_clicks):
    return get_n_clicks(n_clicks)


@app.callback(
    Output('sector', 'value'),
    [Input('sector', 'value'),
     Input('de_select_sector', 'n_clicks'),
     State('g_n_clicks_sector', 'data')])
def set_feature_value(i_values, i_n_clicks, i_g_n_clicks):
    return get_feature_value(i_values, i_n_clicks, i_g_n_clicks, g_all_sector_values, 'sector')


@app.callback(Output('g_n_clicks_repayment_interval', 'data'),
              Input('de_select_repayment_interval', 'n_clicks'))
def on_click(n_clicks):
    return get_n_clicks(n_clicks)


@app.callback(
    Output('repayment_interval', 'value'),
    [Input('repayment_interval', 'value'),
     Input('de_select_repayment_interval', 'n_clicks'),
     State('g_n_clicks_repayment_interval', 'data')])
def set_feature_value(i_values, i_n_clicks, i_g_n_clicks):
    return get_feature_value(i_values, i_n_clicks, i_g_n_clicks, g_all_repayment_interval_values, 'repayment_interval')


@app.callback(Output('g_n_clicks_loan_amount_cat', 'data'),
              Input('de_select_loan_amount_cat', 'n_clicks'))
def on_click(n_clicks):
    return get_n_clicks(n_clicks)


@app.callback(
    Output('loan_amount_cat', 'value'),
    [Input('loan_amount_cat', 'value'),
     Input('de_select_loan_amount_cat', 'n_clicks'),
     State('g_n_clicks_loan_amount_cat', 'data')])
def set_feature_value(i_values, i_n_clicks, i_g_n_clicks):
    return get_feature_value(i_values, i_n_clicks, i_g_n_clicks, g_all_loan_amount_cat_values, 'loan_amount_cat')



@app.callback(Output('g_n_clicks_team_composition', 'data'),
              Input('de_select_team_composition', 'n_clicks'))
def on_click(n_clicks):
    return get_n_clicks(n_clicks)


@app.callback(
    Output('team_composition', 'value'),
    [Input('team_composition', 'value'),
     Input('de_select_team_composition', 'n_clicks'),
     State('g_n_clicks_team_composition', 'data')])
def set_feature_value(i_values, i_n_clicks, i_g_n_clicks):
    return get_feature_value(i_values, i_n_clicks, i_g_n_clicks, g_all_team_composition_values, 'team_composition')


@app.callback(Output('g_n_clicks_lender_cat', 'data'),
              Input('de_select_lender_cat', 'n_clicks'))
def on_click(n_clicks):
    return get_n_clicks(n_clicks)


@app.callback(
    Output('lender_cat', 'value'),
    [Input('lender_cat', 'value'),
     Input('de_select_lender_cat', 'n_clicks'),
     State('g_n_clicks_lender_cat', 'data')])
def set_feature_value(i_values, i_n_clicks, i_g_n_clicks):
    return get_feature_value(i_values, i_n_clicks, i_g_n_clicks, g_all_lender_cat_values, 'lender_cat')


@app.callback(Output('g_n_clicks_term_in_months_cat', 'data'),
              Input('de_select_term_in_months_cat', 'n_clicks'))
def on_click(n_clicks):
    return get_n_clicks(n_clicks)


@app.callback(
    Output('term_in_months_cat', 'value'),
    [Input('term_in_months_cat', 'value'),
     Input('de_select_term_in_months_cat', 'n_clicks'),
     State('g_n_clicks_term_in_months_cat', 'data')])
def set_feature_value(i_values, i_n_clicks, i_g_n_clicks):
    return get_feature_value(i_values, i_n_clicks, i_g_n_clicks, g_all_term_in_months_cat_values, 'term_in_months_cat')

@app.callback(Output('g_n_clicks_continent', 'data'),
              Input('de_select_continent', 'n_clicks'))
def on_click(n_clicks):
    return get_n_clicks(n_clicks)

@app.callback(Output('g_n_clicks_country', 'data'),
              Input('de_select_country', 'n_clicks'))
def on_click(n_clicks):
    return get_n_clicks(n_clicks)

@app.callback(
    Output('continent', 'value'),
    [Input('continent', 'value'),
     Input('de_select_continent', 'n_clicks'),
     State('g_n_clicks_continent', 'data')])
def set_continent(i_continents, i_n_clicks_continent, i_g_n_clicks_continent):
    os.system("clear")
    if i_n_clicks_continent == None:
        i_n_clicks_continent = 0
        i_g_n_clicks_continent = 0
    if i_continents == None:
        l_continents = get_saved_data_feature_value('continent')    
    else:
        l_continents = i_continents
    # print(i_n_clicks_continent)
    # print(i_g_n_clicks_continent)
    if i_n_clicks_continent != i_g_n_clicks_continent:
        # print('b')
        if isinstance(l_continents, list):
            # print('c')
            if len(l_continents) != len(g_all_continent_values):
                # print('d')
                l_continents = save_data_feature_value('continent', g_all_continent_values)
            else:
                # print('e')
                l_continents = save_data_feature_value('continent', [])
    else:
        l_continents = save_data_feature_value('continent', l_continents)
    return l_continents

@app.callback(
    Output('dummy', 'value'),
    [Input('country', 'value')])
def set_measure_1_value(i_countries):
    if i_countries == None:
        l_countries = get_saved_data_feature_value('country')
    else:
        l_countries = i_countries
    if isinstance(l_countries, list):
        l_countries = save_data_feature_value('country', l_countries)
    return l_countries

@app.callback(
    Output('country', 'options'),
    [Input('continent', 'value')])
def set_country_options(i_continent):
    l_df = g_df_grouped.loc[g_df_grouped['continent'].isin(i_continent)]
    return get_options(l_df, 'country')
#


@app.callback(
    Output('country', 'value'),
    [Input('de_select_continent', 'n_clicks'),
    Input('continent', 'value'),
    Input('de_select_country', 'n_clicks'),
    Input('country', 'value'),
    State('g_n_clicks_continent', 'data'),
    State('g_n_clicks_country', 'data')])
def set_country_value(i_n_clicks_continent, i_continents, i_n_clicks_country, i_countries, i_g_n_clicks_continent, i_g_n_clicks_country):
    if i_n_clicks_continent == None:
        i_n_clicks_continent = 0
        i_g_n_clicks_continent = 0
    if i_n_clicks_country == None:
        i_n_clicks_country = 0
        i_g_n_clicks_country = 0
    os.system("clear")
    print(i_n_clicks_country)
    print(i_g_n_clicks_country)
    if i_continents == None:
        l_continents = get_saved_data_feature_value('continent')    
    else:
        l_continents = i_continents
    if i_countries == None:
        l_countries = get_saved_data_feature_value('country')    
    else:
        l_countries = i_countries
    print(l_countries)
    print(len(l_countries))
    print(len(g_all_country_values))
    if i_n_clicks_continent != i_g_n_clicks_continent:
        # print('c')
        if len(l_continents) != len(g_all_continent_values):
            l_countries = save_data_feature_value('country', g_all_country_values)
        else:
            l_countries = save_data_feature_value('country', [])
    elif i_n_clicks_country != i_g_n_clicks_country:
        print('f')
        if len(l_countries) == 0:
            print('g')
            l_countries = save_data_feature_value('country', g_all_country_values)
        elif len(l_countries) != len(g_all_country_values):
            print('h')
            # country
            l_countries = save_data_feature_value('country', g_all_country_values)
        else:
            print('i')
            # country
            l_countries = save_data_feature_value('country', [])
    else:
        l_countries = save_data_feature_value('country', l_countries)
    #
    return l_countries
    # return get_saved_data_feature_value('country')

#
#
#
#
#
if __name__ == '__main__':
    app.run_server(debug=True, port=9001)
