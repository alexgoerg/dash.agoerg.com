import json


def save_data_feature_value(i_feature_name, i_value):
    with open('data/saved_data/' + i_feature_name + '.json') as json_file:
        l_saved_data = json.load(json_file)

    l_saved_data = i_value

    with open('data/saved_data/' + i_feature_name + '.json', 'w') as json_file:
        json.dump(l_saved_data, json_file)

    with open('data/saved_data/' + i_feature_name + '.json') as json_file:
        l_saved_data = json.load(json_file)

    return l_saved_data
