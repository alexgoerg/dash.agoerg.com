import json

def get_saved_data():
    with open('data/saved_data/saved_data.json') as json_file:
        saved_data = json.load(json_file)
    return saved_data