from function.get_saved_data_feature_value import get_saved_data_feature_value
from function.save_data_feature import save_data_feature

def get_feature_value(i_values, i_n_clicks, i_g_n_clicks, i_all_values, i_feature_name):
    """"""
    #
    if i_values == None:
        l_saved_data = get_saved_data_feature_value(i_feature_name)
        l_data = l_saved_data
    else:
        l_data = i_values
    #
    if i_n_clicks == None:
        i_n_clicks = 0
        i_g_n_clicks = 0
    #
    if i_n_clicks != i_g_n_clicks:
        if isinstance(l_data, list):
            if len(l_data) != len(i_all_values):
                l_data = i_all_values
            else:
                l_data = []
    #
    l_data = save_data_feature(i_feature_name, l_data)
    #
    return l_data