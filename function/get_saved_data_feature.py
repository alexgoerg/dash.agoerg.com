import json

def get_saved_data_feature(i_feature_name):
    with open('data/saved_data/' + i_feature_name +'.json') as json_file:
        saved_data = json.load(json_file)
    return saved_data