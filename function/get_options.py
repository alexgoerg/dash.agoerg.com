def get_options(i_df, i_column_name):
    l_df = i_df.groupby(by=i_column_name, as_index=False).agg({'loan_amount': 'sum'})
    l_df.sort_values(by='loan_amount', inplace=True, ascending=False)
    dict_list = []
    for i in l_df.loc[:, i_column_name].unique():
        dict_list.append({'label': i, 'value': i})
    return dict_list