from function.get_saved_data_feature import get_saved_data_feature
from function.save_data_feature import save_data_feature

def get_feature(i_feature_name, i_feature):
    l_saved_data = get_saved_data_feature(i_feature_name)
    if i_feature:
        l_saved_data = save_data_feature(i_feature_name, i_feature)
    return l_saved_data